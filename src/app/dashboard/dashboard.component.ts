import { Component, OnInit } from '@angular/core';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private settingService: SettingService
  ) { }

  ngOnInit(): void {
    this.GetDashboardStepsList();
  }

  GetDashboardStepsList() {
    this.settingService.GetDashboardStepsList().subscribe((response: any) => {
      console.log(response);
    })
  }
}
