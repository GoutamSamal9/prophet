import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FacilityLabourComponent } from './facility-labour/facility-labour.component';
import { IngrediantAndMaterialComponent } from './ingrediant-and-material/ingrediant-and-material.component';
import { MerchantFeeComponent } from './merchant-fee/merchant-fee.component';
import { SharedModule } from './shared/shared.module';
import { AddMaterialComponent } from './add-material/add-material.component';
import { MaterialListComponent } from './material-list/material-list.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { MainMenuComponent } from './main-menu/main-menu.component';


@NgModule({
  declarations: [
    AppComponent,
    ThankYouComponent,
    DashboardComponent,
    FacilityLabourComponent,
    IngrediantAndMaterialComponent,
    MerchantFeeComponent,
    AddMaterialComponent,
    MaterialListComponent,
    IngredientListComponent,
    MainMenuComponent,
    SidebarComponent
  ],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
