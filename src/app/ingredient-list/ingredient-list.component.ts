import { Component, OnInit } from '@angular/core';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css']
})
export class IngredientListComponent implements OnInit {

  ingerdientList= [];
  constructor(
    private settingService: SettingService
  ) { }

  ngOnInit(): void {
    this.GetIngredientList();
  }

  GetIngredientList() {
    this.settingService.GetIngredientList().subscribe((response: any) => {
      console.log(response);
      if(response.Code == '4001') {
        this.ingerdientList = response.ResponseData;
        console.log(this.ingerdientList);
      }
    })
  }
}
