import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SettingMenuComponent } from './components/setting-menu/setting-menu.component';



@NgModule({
  declarations: [
    HeaderComponent, 
    SettingMenuComponent, 
    
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HeaderComponent,
    SettingMenuComponent,
    
  ]
})
export class SharedModule { }
