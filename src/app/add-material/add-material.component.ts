import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'app-add-material',
  templateUrl: './add-material.component.html',
  styleUrls: ['./add-material.component.css']
})
export class AddMaterialComponent implements OnInit {

  materialForm: FormGroup;
  saved: boolean = false;
  selectedFile: File = null;
  fileData: any;
  unitList: any;
  qty: number = 0;
  waste: number = 0;
  price: number = 0;
  adjQty: number = 0;
  totalPrice: number = 0;
  perUnitPrice: number = 0;
  constructor(
    public fb: FormBuilder,
    private settingService: SettingService
  ) {

    this.materialForm = this.fb.group({
      material_name: ['', [Validators.required]],
      supplier_name: ['', [Validators.required]],
      image_path: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      qty: ['', [Validators.required]],
      waste: ['', [Validators.required]],
      price: ['', [Validators.required]],
      sales_tax: ['', [Validators.required]],
      additional_fees: ['', [Validators.required]],
      product_description: ['', [Validators.required]],
    })
  }

  ngOnInit(): void {
    let response = this.settingService.getUnitList();
    response.subscribe((data) => {
      console.log(data);
    });
  }

  get f() {
    return this.materialForm.controls;
  }

  onFileSelected(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.materialForm.get('image_path').setValue(file);
    }
  }

  onChange(event, type) {
    switch (type) {
      case "qty":
        this.qty = event.target.value;
        break;
      case "waste":
        this.waste = event.target.value;
        break;
      case "price":
        this.price = event.target.value;
        break;
    }
    this.adjQty = this.qty - (this.waste / 100 * this.qty);
    this.perUnitPrice = +(this.price / this.qty).toFixed(2);
    this.totalPrice = this.price;
  }


  handelIngredientSubmit() {

    // const formData = new FormData();
    // formData.append('file', this.materialForm.get('image_path').value);
    let data = {
      "material_name": this.materialForm.value.material_name,
      "supplier_name": this.materialForm.value.supplier_name,
      "image_path": "",
      "brand": this.materialForm.value.brand,
      "qty": this.materialForm.value.qty,
      "waste": this.materialForm.value.waste,
      "price": this.materialForm.value.price,
      "sales_tax": this.materialForm.value.sales_tax,
      "additional_fees": this.materialForm.value.additional_fees,
      "product_description": this.materialForm.value.product_description,
      "created_by": "Bijay",
      "activity_header": "Added Ingredient",
      "activity_desc": "test ingredient for test supplier and test brand",
      "user_id": "1",
      "unit_id": 1,
      "total_price": this.totalPrice,
      "price_per_unit": this.perUnitPrice,
      "active_status": "1"
    }
    if (this.materialForm.status == "VALID") {
      this.settingService.insertUpdateMaterial(data).subscribe((response: any) => {
        if(response) {
          this.materialForm.reset();
          this.saved = true;
          this.adjQty =  0;
          this.perUnitPrice =  0;
          this.price =  0;
        }
        
      })
    }
  }

}
