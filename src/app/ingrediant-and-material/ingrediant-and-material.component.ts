import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'app-ingrediant-and-material',
  templateUrl: './ingrediant-and-material.component.html',
  styleUrls: ['./ingrediant-and-material.component.css']
})
export class IngrediantAndMaterialComponent implements OnInit {

  ingredientsForm: FormGroup;
  saved: boolean = false;
  selectedFile: File = null;
  fileData: any;
  unitList: any;
  qty: number = 0;
  waste: number = 0;
  price: number = 0;
  adjQty: number = 0;
  totalPrice: number = 0;
  perUnitPrice: number = 0;
  constructor(
    public fb: FormBuilder,
    private settingService: SettingService
  ) {

    this.ingredientsForm = this.fb.group({
      ingredient_name: ['', [Validators.required]],
      supplier_name: ['', [Validators.required]],
      image_path: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      qty: ['', [Validators.required]],
      waste: ['', [Validators.required]],
      price: ['', [Validators.required]],
      sales_tax: ['', [Validators.required]],
      additional_fees: ['', [Validators.required]],
      product_description: ['', [Validators.required]],
    })
  }

  ngOnInit(): void {
    let response = this.settingService.getUnitList();
    response.subscribe((data) => {
      console.log(data);
    });
    // this.ingredientsForm = this.fb.group({
    //   image_path: ['']
    // });
  }

  get f() {
    return this.ingredientsForm.controls;
  }

  onFileSelected(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.ingredientsForm.get('image_path').setValue(file);
    }
  }

  onChange(event, type) {
    switch (type) {
      case "qty":
        this.qty = event.target.value;
        break;
      case "waste":
        this.waste = event.target.value;
        break;
      case "price":
        this.price = event.target.value;
        break;
    }
    this.adjQty = this.qty - (this.waste / 100 * this.qty);
    this.perUnitPrice = +(this.price / this.qty).toFixed(2);
    this.totalPrice = this.price;
  }


  handelIngredientSubmit() {

    // const formData = new FormData();
    // formData.append('file', this.ingredientsForm.get('image_path').value);
    let data = {
      "ingredient_name": this.ingredientsForm.value.ingredient_name,
      "supplier_name": this.ingredientsForm.value.supplier_name,
      "image_path": "",
      "brand": this.ingredientsForm.value.brand,
      "qty": this.ingredientsForm.value.qty,
      "waste": this.ingredientsForm.value.waste,
      "price": this.ingredientsForm.value.price,
      "sales_tax": this.ingredientsForm.value.sales_tax,
      "additional_fees": this.ingredientsForm.value.additional_fees,
      "product_description": this.ingredientsForm.value.product_description,
      "created_by": "Bijay",
      "activity_header": "Added Ingredient",
      "activity_desc": "test ingredient for test supplier and test brand",
      "user_id": "1",
      "unit_id": 1,
      "total_price": this.totalPrice,
      "price_per_unit": this.perUnitPrice,
      "active_status": "1"
    }
    if (this.ingredientsForm.status == "VALID") {
      this.settingService.inputIngredient(data).subscribe((response: any) => {
        this.ingredientsForm.reset();
        this.saved = true;
      })
    }
  }

}
