import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngrediantAndMaterialComponent } from './ingrediant-and-material.component';

describe('IngrediantAndMaterialComponent', () => {
  let component: IngrediantAndMaterialComponent;
  let fixture: ComponentFixture<IngrediantAndMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngrediantAndMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngrediantAndMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
