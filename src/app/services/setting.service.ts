import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(
    public http: HttpClient,
  ) { }

  /**
    * @description
    * insert Facility Labour Data
    *
    * @memberof SettingService
    */
  insertUpdateFacilityLabour(data: any) {
    return this.http.post(`${environment.server_url}/api/Settings/InsertUpdateFacilityLabour`, data);
  }

  /**
    * @description
    * get unit list 
    *
    * @memberof SettingService
    */

  getUnitList() {
    return this.http.get(`${environment.server_url}/api/Settings/GetUnitList`);
  }


  /**
    * @description
    * insert Merchant Data
    *
    * @memberof SettingService
    */
  insertUpdateMerchantFee(data: any) {
    return this.http.post(`${environment.server_url}/api/Settings/InsertUpdateMerchantFee`, data);
  }

  inputIngredient(data: any) {
    return this.http.post(`${environment.server_url}/api/Ingredient/InsertUpdateIngredient`, data);
  }

  insertUpdateMaterial(data: any) {
    return this.http.post(`${environment.server_url}/api/Ingredient/insertUpdateMaterial`, data);
  }

  GetIngredientList() {
    return this.http.get(`${environment.server_url}/api/Ingredient/GetIngredientList?id=0&user_id=1`);
  }
  
  GetMaterialList() {
    return this.http.get(`${environment.server_url}/api/Ingredient/GetMaterialList?id=0&user_id=1`);
  }

  GetDashboardStepsList() {
    return this.http.get(`${environment.server_url}/api/Settings/GetDashboardStepsList?user_id=1`);
  }
}
