import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'app-merchant-fee',
  templateUrl: './merchant-fee.component.html',
  styleUrls: ['./merchant-fee.component.css']
})
export class MerchantFeeComponent implements OnInit {

  merchantForm: FormGroup;
  saved: boolean = false;
  constructor(
    public fb: FormBuilder,
    private settingService: SettingService
  ) {
    this.merchantForm = this.fb.group({
      percentage_value: ['', [Validators.required]],
      fixed_value: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
  }

  get f() {
    return this.merchantForm.controls;
  }

  /**
    * @description
    * insert Merchant Data
    *
    * @memberof MerchantFeeComponent
    */
  onSubmit() {
    let data = {
      "percentage_value": this.merchantForm.value.percentage_value,
      "fixed_value": this.merchantForm.value.fixed_value,
      "created_by": "Bijay",
      "activity_header": "Added Merchant Fee",
      "activity_desc": "Percentage 10.12 and Fixed 12.09",
      "user_id": "1"
    }
    if (this.merchantForm.status == "VALID") {
      this.settingService.insertUpdateMerchantFee(data).subscribe((response: any) => {
        this.merchantForm.reset();
        this.saved = true;
      })
    }
  }
}
