import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantFeeComponent } from './merchant-fee.component';

describe('MerchantFeeComponent', () => {
  let component: MerchantFeeComponent;
  let fixture: ComponentFixture<MerchantFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchantFeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
