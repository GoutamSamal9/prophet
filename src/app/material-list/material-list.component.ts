import { Component, OnInit } from '@angular/core';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'app-material-list',
  templateUrl: './material-list.component.html',
  styleUrls: ['./material-list.component.css']
})
export class MaterialListComponent implements OnInit {

  materialList= [];
  constructor(
    private settingService: SettingService
  ) { }

  ngOnInit(): void {
    this.GetMaterialList();
  }

  GetMaterialList() {
    this.settingService.GetMaterialList().subscribe((response: any) => {
      console.log(response);
      if(response.Code == '4001') {
        this.materialList = response.ResponseData;
        console.log(this.materialList);
      }
    })
  }
}