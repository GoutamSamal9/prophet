import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddMaterialComponent } from './add-material/add-material.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FacilityLabourComponent } from './facility-labour/facility-labour.component';
import { IngrediantAndMaterialComponent } from './ingrediant-and-material/ingrediant-and-material.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { MaterialListComponent } from './material-list/material-list.component';
import { MerchantFeeComponent } from './merchant-fee/merchant-fee.component';
import { ThankYouComponent } from './thank-you/thank-you.component';


const routes: Routes = [
  { path: '', redirectTo: 'create-account', pathMatch: 'full' },
  {
    path: 'create-account',
    loadChildren: () =>
      import('./create-account/create-account.module').then((m) => m.CreateAccountModule),
  },
  {
    path: 'sign-in',
    loadChildren: () =>
      import('./sign-in/sign-in.module').then((m) => m.SignInModule),
  },
  {
    path: 'thank-you/:email',
    component: ThankYouComponent
  },
  {
    path: 'facility-labour',
    component: FacilityLabourComponent
  },
  {
    path: 'ingredient-and-material',
    component: IngrediantAndMaterialComponent
  },
  {
    path: 'ingredient-list',
    component: IngredientListComponent
  },
  {
    path: 'add-material',
    component: AddMaterialComponent
  },
  {
    path: 'material-list',
    component: MaterialListComponent
  },
  {
    path: 'merchant-fee',
    component: MerchantFeeComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
