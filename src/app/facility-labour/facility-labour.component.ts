import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SettingService } from '../services/setting.service';

@Component({
  selector: 'app-facility-labour',
  templateUrl: './facility-labour.component.html',
  styleUrls: ['./facility-labour.component.css']
})
export class FacilityLabourComponent implements OnInit {

  form: FormGroup;
  prod_days_per_month: number = 0;
  prod_hour_per_month: number = 0;
  facility_cost_per_month: number = 0;
  labour_cost_per_hour: number = 0;
  saved: boolean = false;
  constructor(
    public fb: FormBuilder,
    private settingService: SettingService
  ) {
    this.form = this.fb.group({
      prod_days_per_month: ['', [Validators.required]],
      prod_hour_per_month: ['', [Validators.required]],
      facility_cost_per_month: ['', [Validators.required]],
      facility_cost_per_hour: [''],
      labour_cost_per_month: [''],
      labour_cost_per_hour: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {

  }

  get f() {
    return this.form.controls;
  }

  onChange_prod_days_per_month(event) {
    this.prod_days_per_month = event.target.value;
    this.facility_cost_per_hour();
  }
  onChange_prod_hour_per_month(event) {
    this.prod_hour_per_month = event.target.value;
    this.facility_cost_per_hour();
  }
  onChange_facility_cost_per_month(event) {
    this.facility_cost_per_month = event.target.value;
    this.facility_cost_per_hour();
  }
  onChange_labour_cost_per_hour(event) {
    this.labour_cost_per_hour = event.target.value;
    this.labour_cost_per_month();
  }

  facility_cost_per_hour() {
    this.form.patchValue({ 'facility_cost_per_hour': this.facility_cost_per_month / (this.prod_days_per_month * this.prod_hour_per_month) });
  }

  labour_cost_per_month() {
    this.form.patchValue({ 'labour_cost_per_month': this.labour_cost_per_hour * (this.prod_days_per_month * this.prod_hour_per_month) });
  }

  /**
    * @description
    * insert Facility Labour Data
    *
    * @memberof FacilityLabourComponent
    */
  onSubmit() {
    let data = {
      prod_days_per_month: this.form.value.prod_days_per_month,
      prod_hour_per_month: this.form.value.prod_hour_per_month,
      facility_cost_per_month: this.form.value.facility_cost_per_month,
      facility_cost_per_hour: this.form.value.facility_cost_per_hour,
      labour_cost_per_month: this.form.value.labour_cost_per_month,
      labour_cost_per_hour: this.form.value.labour_cost_per_hour,
      production_cost_per_hour: 30,
      created_by: "Admin",
      active_status: "1",
      activity_header: "Added Facility & Labour",
      activity_desc: "test",
      user_id: "1"
    }
    if (this.form.status == 'VALID') {
      this.settingService.insertUpdateFacilityLabour(data).subscribe((response: any) => {
        this.form.reset();
        this.saved = true;
      });
    }
  }
}
