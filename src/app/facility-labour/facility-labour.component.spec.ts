import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityLabourComponent } from './facility-labour.component';

describe('FacilityLabourComponent', () => {
  let component: FacilityLabourComponent;
  let fixture: ComponentFixture<FacilityLabourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityLabourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityLabourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
