import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

  mainMenu: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  changeMainMenu() {
    this.mainMenu = true;
  }
}
